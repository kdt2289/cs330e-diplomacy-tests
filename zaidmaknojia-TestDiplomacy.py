#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read_line, diplomacy_eval, diplomacy_print, diplomacy_solve, Army

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        line = diplomacy_read_line(s)
        self.assertEqual(line,  ['A', 'Madrid', 'Hold', ""])

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        line = diplomacy_read_line(s)
        self.assertEqual(line,  ["B", "Barcelona","Move","Madrid"])

    def test_read_3(self):
        s = "C Houston Support G\n"
        line = diplomacy_read_line(s)
        self.assertEqual(line,  ["C", "Houston","Support","G"])

    # ----
    # eval
    # ----
    def test_eval_1(self):
       armies = dict({"C": Army(army_name='C',location='London', action='Move', aux='Madrid')})
       diplomacy_eval(armies)
       self.assertEqual(armies, dict({"C": Army(army_name='C',location='Madrid', action='Move', aux='Madrid')}))

    def test_eval_2(self):
        armies = dict({"C": Army(army_name='C',location='London', action='Move', aux='Madrid'), 
                       "A": Army(army_name='A',location='Austin', action='Hold')})
        diplomacy_eval(armies)
        self.assertEqual(armies, dict({"C": Army(army_name='C',location='Madrid', action='Move', aux='Madrid'), 
                                       "A": Army(army_name='A',location='Austin', action='Hold')}))

    def test_eval_3(self):
        armies = dict({"Z": Army(army_name='Z',location='Mumbai', action='Move', aux='Shanghai'), 
                       "A": Army(army_name='A',location='Shanghai', action='Hold'),
                       "G": Army(army_name='G',location='London', action='Support', aux="A")})
        diplomacy_eval(armies)
        self.assertEqual(armies, dict({"Z": Army(army_name='Z',location='Shanghai', action='Move', aux='Shanghai'), 
                                       "A": Army(army_name='A',location='Shanghai', action='Hold'),
                                       "G": Army(army_name='G',location='London', action='Support', aux="A")}))
    

    # -----
    # print
    # -----
    def test_print_1(self):
        w = StringIO()
        armies = dict({"C": Army(army_name='C',location='London', action='Move', aux='Madrid')})
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "C London\n")

    def test_print_2(self):
        w = StringIO()
        armies = dict({"C": Army(army_name='C',location='London', action='Move', aux='Madrid'), 
                       "A": Army(army_name='A',location='Austin', action='Hold')})
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A Austin\nC London\n")

    def test_print_3(self):
        w = StringIO()
        armies = dict({"Z": Army(army_name='Z',location='Mumbai', action='Move', aux='Shanghai'), 
                       "A": Army(army_name='A',location='Shanghai', action='Hold'),
                       "G": Army(army_name='G',location='London', action='Support', aux="A")})
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A Shanghai\nG London\nZ Mumbai\n")


    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO("A Madrid Hold \nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Paris Move London\nE Austin Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE Austin\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL                 44      0      2      0   100%
"""
