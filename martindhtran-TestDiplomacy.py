#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 
# Avirat A. Thakor and Martin D. Tran
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----
    def test_read_1(self):
        s = ""
        commands = diplomacy_read(s)
        self.assertEqual(commands,  [])
    
    def test_read_2(self):
        s = "A Madrid Hold"
        commands = diplomacy_read(s)
        self.assertEqual(commands,  [["A", "Madrid", "Hold"]])

    def test_read_3(self):
        s = "A Madrid Hold \n B Barcelona Move Madrid \n C London Support B"
        commands = diplomacy_read(s)
        correct = [["A", "Madrid", "Hold"], \
                   ["B", "Barcelona", "Move", "Madrid"], \
                       ["C", "London", "Support", "B"]]
        self.assertEqual(commands,  correct)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], \
                   ["B", "Miami", "Move", "Madrid"], \
                       ["C", "London", "Support", "B"]])
        self.assertEqual(v, [["A", "[dead]"], ["B", "Madrid"], ["C", "London"]])
    
    def test_eval_2(self):
        v = diplomacy_eval([])
        self.assertEqual(v, [])
    
    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Madrid", "Move", "Barcelona"]])
        self.assertEqual(v, [["A", "Madrid"], ["B", "Barcelona"]])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [])
        self.assertEqual(w.getvalue(), "")
    
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid"]])
        self.assertEqual(w.getvalue(), "A Madrid\n")
    
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid"], ["B", "[dead]"]])
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\n")

    # -----
    # solve
    # -----
    
    def test_solve_1(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Paris Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve_3(self):
        r = StringIO("A Moscow Hold\nB Rome Move Moscow\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Moscow\nC London\n")
    
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n" 
                     + "C London Move Madrid\nD Paris Support B\n" + 
                     "E Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), ("A [dead]\nB [dead]\nC [dead]\nD Paris" 
                                         + "\nE Austin\n"))
                         
    def test_solve_5(self):
        r = StringIO("A Madrid Hold \n B Barcelona Move Madrid \n" + 
                     "C London Move Madrid \n D Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), ("A [dead]\nB Madrid\nC [dead]" + 
                                       "\nD Paris\n"))

    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n" + 
                     "C London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), ("A [dead]\nB [dead]\nC [dead]\n"))   
    
    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n" + 
                     "C London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), ("A [dead]\nB [dead]\nC [dead]\n" +
                         "D [dead]\n"))

    def test_solve_8(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Hold\n" + 
                    "D Austin Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), ("A Madrid\nB Barcelona\nC London\n" + 
                                       "D Austin\n"))

    def test_solve_9(self):
        r = StringIO("A Madrid Support B\nB Rome Move Vienna")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Vienna\n")

    def test_solve_10(self):
        r = StringIO("A Madrid Support B\nB Rome Move Vienna\n" + 
                     "C Istanbul Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Vienna\nC [dead]\n")

    def test_solve_11(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Berlin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Berlin\n")

    def test_solve_12(self):
        r = StringIO("A Madrid Move Barcelona\nE Oslo Support C\n" +
                     "B Barcelona Support C\nC KansasCity Hold\n" +
                     "D Birmingham Move KansasCity\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC KansasCity\n" + 
                                       "D [dead]\nE Oslo\n")

    def test_solve_13(self):
        r = StringIO("A Madrid Move Barcelona\nE Oslo Support C\n" +
                     "B Barcelona Hold\nC KansasCity Hold\n" + 
                     "D Birmingham Move KansasCity\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC KansasCity\n" +
                                       "D [dead]\nE Oslo\n")

    def test_solve_14(self):
        r = StringIO("A Madrid Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\n")

    def test_solve_15(self):
        r = StringIO("A Paris Move Rome\nB Rome Support A\n"+
                     "C Bangkok Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Bangkok\n")

    def test_solve_16(self):
        r = StringIO("A Paris Support B\nB Rome Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB Rome\n")

    def test_solve_17(self):
        r = StringIO("A Paris Hold\nB Seoul Move Paris\nC Moscow Support A\n" +
                     "D Warsaw Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Moscow\n" +
                                       "D Warsaw\n")

    def test_solve_18(self):
        r = StringIO("A Paris Hold\nB Seoul Move Paris\nC Moscow Support A\n" +
                     "D Warsaw Support B\nE Austin Move Moscow\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Paris\nC [dead]\n" + 
                                       "D Warsaw\nE [dead]\n")


# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()

